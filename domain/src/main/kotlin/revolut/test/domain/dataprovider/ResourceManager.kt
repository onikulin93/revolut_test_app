package revolut.test.domain.dataprovider

interface ResourceManager {

    fun getStringArray(id: Int): Array<String>

}