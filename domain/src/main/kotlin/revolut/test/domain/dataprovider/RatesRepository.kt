package revolut.test.domain.dataprovider

import io.reactivex.Single
import revolut.test.domain.model.RateList

interface RatesRepository {

    fun loadRates(base: String): Single<RateList>
}