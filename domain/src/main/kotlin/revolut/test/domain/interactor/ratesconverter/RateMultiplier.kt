package revolut.test.domain.interactor.ratesconverter

import javax.inject.Qualifier

@Qualifier
annotation class RateMultiplier