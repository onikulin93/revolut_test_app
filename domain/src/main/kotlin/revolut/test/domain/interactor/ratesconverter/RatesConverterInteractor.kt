package revolut.test.domain.interactor.ratesconverter

import io.reactivex.Observable
import io.reactivex.Single
import revolut.test.domain.model.Rate
import revolut.test.domain.model.RateList
import revolut.test.domain.usecase.rates.LoadRatesUseCase
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.properties.Delegates

data class UpdateInfo(
        val heading: Rate,
        val multiplier: Float,
        val rateList: List<Rate>
)

class RatesConverterInteractor @Inject constructor(
        @BaseRateQualifier
        private var baseRate: String,
        @RateMultiplier
        private var rateMultiplier: Float,
        private val loadRatesUseCase: LoadRatesUseCase
) {

    private val configChangeLock = Object()
    private val cachedDataLock = Object()

    private var cachedData: RateList? = null

    private var observable by Delegates.notNull<Observable<RateList>>()
    private var positionsOrderMap: MutableMap<String, Int>? = null

    init {
        internalStartTickTimer()
    }

    fun onBaseRateChanged(baseRate: String, rateMultiplier: Float) {
        synchronized(configChangeLock) {
            this.baseRate = baseRate
            this.rateMultiplier = rateMultiplier
        }
    }

    fun onRateMultiplierChanged(rateMultiplier: Float) {
        synchronized(configChangeLock) {
            this.rateMultiplier = rateMultiplier
        }
    }

    fun updates(): Observable<UpdateInfo> {
        return observable.map(::updateInfo)
    }

    fun stopUpdates(): Single<UpdateInfo> {
        return synchronized(cachedDataLock) {
            cachedData?.let { Single.just(it) }
                    ?.map { rateList ->
                        synchronized(configChangeLock) {
                            positionsOrderMap?.let {
                                sortByPositions(rateList, it)
                            } ?: {
                                rateList
                            } ()
                        }
                    }
                    ?.map(::updateInfo) ?: TODO()
        }
    }

    fun constructPositionsMap(newLeadingRate: Rate, oldLeadingRate: Rate, oldRateList: List<Rate>) {
        synchronized(configChangeLock) {
            positionsOrderMap?.let { localMap ->
                val position = localMap[newLeadingRate.name]
                position?.let {
                    localMap.keys.forEach { key ->
                        localMap[key]?.takeIf { it > position }?.let {
                            localMap[key] = it - 1
                        }
                    }
                    localMap.remove(newLeadingRate.name)
                    localMap.keys.forEach { key ->
                        localMap[key]?.let { value ->
                            localMap[key] = value + 1
                        }
                    }
                    localMap[newLeadingRate.name]= 0
                }
            } ?: {
                val localMap = mutableMapOf<String, Int>()
                var position = 0
                localMap[newLeadingRate.name] = position++
                localMap[oldLeadingRate.name] = position++
                oldRateList.toMutableList().apply {
                    find { it.name == oldLeadingRate.name }?.let(::remove)
                    find { it.name == newLeadingRate.name }?.let(::remove)
                }.forEachIndexed { _, rate -> localMap[rate.name] = position++ }
                positionsOrderMap = localMap
            }()
        }
    }

    private fun updateInfo(rateList: RateList): UpdateInfo {
        return synchronized(configChangeLock) {
            UpdateInfo(
                    heading = Rate(baseRate, 1f),
                    rateList = rateList.rateList,
                    multiplier = rateMultiplier
            )
        }
    }

    private fun internalStartTickTimer() {
        observable = Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(Long.MAX_VALUE / 2)
                .flatMapSingle { loadRatesUseCase.load(baseRate) }
                .map { rateList ->
                    synchronized(configChangeLock) {
                        positionsOrderMap?.let {
                            sortByPositions(rateList, it)
                        } ?: {
                            rateList
                        } ()
                    }
                }.doOnNext {
                    synchronized(cachedDataLock) {
                        cachedData = it
                    }
                }
    }

    private fun sortByPositions(rateList: RateList, positionMap: Map<String, Int>): RateList {
        return rateList.copy(rateList = rateList.rateList.sortedWith(
                Comparator { rate1, rate2 ->
                    val position1 = positionMap.getOrElse(rate1.name) { 0 }
                    val position2 = positionMap.getOrElse(rate2.name) { 0 }
                    return@Comparator position1 - position2
                }))
    }
}