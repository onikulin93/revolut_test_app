package revolut.test.domain.model

data class Rate(
        val name: String,
        val rate: Float
)