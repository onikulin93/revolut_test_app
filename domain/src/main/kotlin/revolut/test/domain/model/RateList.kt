package revolut.test.domain.model

data class RateList(
        val base: String,
        val rateList: List<Rate>
)