package revolut.test.domain.usecase.rates

import io.reactivex.Single
import revolut.test.domain.dataprovider.RatesRepository
import revolut.test.domain.model.RateList
import javax.inject.Inject

class LoadRatesUseCase @Inject constructor(
        private val repository: RatesRepository
) {

    fun load(base: String): Single<RateList> = repository.loadRates(base)

}