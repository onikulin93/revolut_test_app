package revolut.test.data.network.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import revolut.test.data.network.response.RatesResponse

interface RatesApi {

    @GET("latest")
    fun loadRates(@Query("base") base: String): Single<RatesResponse>
}