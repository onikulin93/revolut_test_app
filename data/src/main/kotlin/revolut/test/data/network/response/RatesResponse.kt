package revolut.test.data.network.response

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import revolut.test.data.model.NWRateList

class RatesResponse(
        @SerializedName("base")
        val base: String? = null,
        @SerializedName("date")
        val date: DateTime? = null,
        @SerializedName("rates")
        val rates: NWRateList? = null
)