package revolut.test.data.network.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import revolut.test.data.exception.ConvertException
import java.lang.reflect.Type

class DateTimeDeserializer : JsonDeserializer<DateTime> {

    companion object {
        private val dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DateTime {
        json ?: throw ConvertException("DateTime element is null")
        val jsonString = json.asString ?: throw ConvertException("DateTime element has wrong format $json")
        return dateFormatter.parseDateTime(jsonString)
    }
}