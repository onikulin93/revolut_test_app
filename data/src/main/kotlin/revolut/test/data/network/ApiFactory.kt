package revolut.test.data.network

import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

class ApiFactory (
        private val baseUrl: String,
        private val okHttpClient: OkHttpClient,
        private val converterFactory: Converter.Factory,
        private val callAdapterFactory: CallAdapter.Factory
) {

    fun <T> create(clazz: Class<T>): T {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
                .create(clazz)
    }
}
