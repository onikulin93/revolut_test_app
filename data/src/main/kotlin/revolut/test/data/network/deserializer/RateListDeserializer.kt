package revolut.test.data.network.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import revolut.test.data.exception.ConvertException
import revolut.test.data.model.NWRate
import revolut.test.data.model.NWRateList
import revolut.test.data.util.mapOrSkip
import java.lang.reflect.Type

class RateListDeserializer: JsonDeserializer<NWRateList> {

    override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
    ): NWRateList {
        json ?: throw ConvertException("DateTime element is null")
        context ?: throw ConvertException("Null context")

        val jsonObject = json.asJsonObject ?: throw ConvertException("RateList element has wrong format $json")

        val list = jsonObject.keySet().mapOrSkip { convertPropertyMemberToRate(it, jsonObject[it]) }
        return NWRateList(list)
    }

    private fun convertPropertyMemberToRate(name: String, element: JsonElement?): NWRate {
        element ?: throw ConvertException("element is null")
        val float = try {
            element.asFloat
        } catch (e : NumberFormatException) {
            throw ConvertException("RateListDeserializer", e)
        }
        return NWRate(name, float)
    }

}