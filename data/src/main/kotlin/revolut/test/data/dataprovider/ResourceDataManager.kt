package revolut.test.data.dataprovider

import android.content.Context
import revolut.test.domain.dataprovider.ResourceManager
import javax.inject.Inject

class ResourceDataManager @Inject constructor(
        private val context: Context
) : ResourceManager {

    override fun getStringArray(id: Int): Array<String> {
        return context.resources.getStringArray(id)
    }
}