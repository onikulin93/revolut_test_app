package revolut.test.data.dataprovider

import io.reactivex.Single
import revolut.test.data.exception.ConvertException
import revolut.test.data.network.api.RatesApi
import revolut.test.data.network.response.RatesResponse
import revolut.test.domain.model.Rate
import revolut.test.domain.model.RateList
import revolut.test.domain.dataprovider.RatesRepository
import javax.inject.Inject

class RatesDataRepository @Inject constructor(
        private val ratesApi: RatesApi
): RatesRepository {

    override fun loadRates(base: String): Single<RateList> {
        return ratesApi.loadRates(base).map(::convert)
    }

    private fun convert(ratesResponse: RatesResponse): RateList {
        val base = ratesResponse.base ?: throw ConvertException("base is null")
        val rateList = ratesResponse.rates?.rateList ?: throw ConvertException("rates list is null")

        return RateList(base, rateList.map { (name, rate) -> Rate(name, rate) })
    }
}