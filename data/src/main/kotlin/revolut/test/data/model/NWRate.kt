package revolut.test.data.model

data class NWRate(
        val name: String,
        val rate: Float
)