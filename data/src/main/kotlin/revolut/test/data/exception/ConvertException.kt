package revolut.test.data.exception

import java.lang.RuntimeException

class ConvertException : RuntimeException {

    constructor(message: String) : super(message)

    constructor(message: String, cause: Throwable): super(message, cause)
}