package revolut.test.data.util

import android.util.Log
import revolut.test.data.exception.ConvertException

inline fun <T, R> maybeConvert(item: T?, converter: (T) -> R): R? = item?.let {
    try {
        converter.invoke(it)
    } catch (e: ConvertException) {
        Log.e("maybeConvert", "", e)
        null
    }
}

inline fun <E, T: Any> Iterable<E>.mapOrSkip(convertFun: (E) -> T?): List<T> =
        mapNotNull { maybeConvert(it, convertFun) }