package android.support.v7.widget

import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator

class RecyclerProxy {

    companion object {

        fun smoothScrollBy(recyclerView: RecyclerView, dx: Int, dy: Int,
                           duration: Int, interpolator: Interpolator = DecelerateInterpolator()) {
            recyclerView.mViewFlinger.smoothScrollBy(dx, dy, duration, interpolator)
        }
    }
}