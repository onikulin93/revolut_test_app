package revolut.test.ui.activity.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import revolut.test.R
import revolut.test.ui.fragment.ext.addFragmentToBackStack
import revolut.test.ui.fragment.ConverterFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            switchToConverterFragment()
        }
    }

    private fun switchToConverterFragment() {
        with(ConverterFragment.Companion) {
            addFragmentToBackStack(
                    containerViewId = R.id.activity_main_fragment_holder,
                    fragment = newInstance(),
                    tag = TAG,
                    addToBackStack = supportFragmentManager
                            .findFragmentById(R.id.activity_main_fragment_holder) != null
            )
        }
    }
}