package revolut.test.ui.viewinterface

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import revolut.test.uimodel.RateItem

interface ConverterView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun subscribeToSlideAnimation(rateItem: RateItem)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateRateList(rateList: List<RateItem>)

}