package revolut.test.ui.view

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection

class InterceptView @JvmOverloads constructor(context: Context,
                                              attrs: AttributeSet? = null,
                                              defStyle: Int = 0) : View(context, attrs, defStyle) {


    override fun onCreateInputConnection(outAttrs: EditorInfo?): InputConnection? {
        return outAttrs?.let {
            it.inputType = it.inputType or InputType.TYPE_CLASS_NUMBER
            super.onCreateInputConnection(it)
        }
    }

}