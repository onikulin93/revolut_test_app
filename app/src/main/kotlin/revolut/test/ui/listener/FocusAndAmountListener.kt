package revolut.test.ui.listener

import android.view.View

interface FocusAndAmountListener: View.OnFocusChangeListener {

    fun onAmountChanged(amount: Float)

}