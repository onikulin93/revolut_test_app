package revolut.test.ui.recycler.rates

import android.support.v7.util.ListUpdateCallback
import android.support.v7.widget.RecyclerView

class RateListUpdateCallback(
        private val adapter: RecyclerView.Adapter<RateViewHolder>
) : ListUpdateCallback {

    internal var isFirstItemFreezed = false

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        if (isFirstItemFreezed && position == 0 && count == 1) {
            return
        }
        adapter.notifyItemRangeChanged(position, count, payload)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        adapter.notifyItemMoved(fromPosition, toPosition);
    }

    override fun onInserted(position: Int, count: Int) {
        adapter.notifyItemRangeInserted(position, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        adapter.notifyItemRangeRemoved(position, count);
    }

}