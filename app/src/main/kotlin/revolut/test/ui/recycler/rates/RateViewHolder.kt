package revolut.test.ui.recycler.rates

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import revolut.test.R
import revolut.test.ui.listener.FocusAndAmountListener
import revolut.test.uimodel.RateItem
import revolut.test.util.simplelisteners.SimpleTextWatcher
import kotlinx.android.synthetic.main.item_rate.view.item_rate_edit_field as rateEditField
import kotlinx.android.synthetic.main.item_rate.view.item_rate_image as rateImageView
import kotlinx.android.synthetic.main.item_rate.view.item_rate_info as rateInfoView
import kotlinx.android.synthetic.main.item_rate.view.item_rate_name as rateNameView

class RateViewHolder(private val rootView: View) : RecyclerView.ViewHolder(rootView) {

    companion object {
        const val KEY_CLICKED = R.id.test
    }

    private var textWatcher: TextWatcher? = null

    fun fullBind(rateItem: RateItem) {
        with(rootView) {
            rateImageView.setImageResource(rateItem.rateInfo.imageResource)
            rateNameView.text = rateItem.rate.name
            rateInfoView.text = context.getString(rateItem.rateInfo.description)

            textWatcher?.let {
                rateEditField.removeTextChangedListener(it)
            }
            val result = context.getString(R.string.template_rate_format).format(rateItem.rate.rate * rateItem.amount)
            rateEditField.setText(result)
        }
    }

    fun bindClickListener(rateItem: RateItem, listener: (RateItem) -> Unit) {
        with(rootView) {
            setOnClickListener {
                //setTag(KEY_CLICKED, true)
                //listener.invoke(rateItem)
            }
            rateEditField.setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    setTag(KEY_CLICKED, true)
                    listener.invoke(rateItem)
                }
                false
            }
        }
    }

    fun bindFocusAndRateListener(focusAndAmountListener: FocusAndAmountListener) {
        with(rootView.rateEditField) {
            textWatcher = SimpleTextWatcher {

                if (it.isEmpty()) {
                    focusAndAmountListener.onAmountChanged(0f)
                }

                it
                        .replace(",", ".")
                        .toFloatOrNull()
                        ?.let { float ->
                            focusAndAmountListener.onAmountChanged(float)
                        }
            }
            addTextChangedListener(textWatcher)
            onFocusChangeListener = focusAndAmountListener
            if (isFocused) {
                focusAndAmountListener.onFocusChange(this, true)//to trigger freeze
            } else {
                //setKeyboardFocusView()
            }
        }
    }

    fun partialBind(bundle: Bundle) {
        bindRateInternal(bundle)
    }

    private fun bindRateInternal(bundle: Bundle) {
        val rate = bundle
                .getFloat(RateAdapter.KEY_RATE_CHANGED_TO, -1f)
                .takeIf { it != -1f } ?: return
        val amount = bundle
                .getFloat(RateAdapter.KEY_AMOUNT_CHANGED_TO, -1f)//todo can be negative?
                .takeIf { it != -1f } ?: return

        textWatcher?.let {
            rootView.rateEditField.removeTextChangedListener(it)
        }

        val selectionStart = rootView.rateEditField.selectionStart
        val selectionEnd = rootView.rateEditField.selectionEnd

        val result = rootView.context.getString(R.string.template_rate_format).format(rate * amount)
        val oldTextLength = rootView.rateEditField.text.length
        rootView.rateEditField.setText(result)

        if (rootView.rateEditField.isFocused && oldTextLength == rootView.rateEditField.text.length) {
            rootView.rateEditField.setSelection(selectionStart, selectionEnd)
        } else if (rootView.rateEditField.isFocused){
            rootView.rateEditField.setSelection(rootView.rateEditField.text.length - 1)
        }

    }

    fun unbindClickListeners() {
        with(rootView) {
            setOnClickListener(null)
            rateEditField.setOnTouchListener(null)
        }
    }
}