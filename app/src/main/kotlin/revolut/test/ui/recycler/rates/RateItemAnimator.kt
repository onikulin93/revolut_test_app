package revolut.test.ui.recycler.rates

import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.RecyclerProxy
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.Property
import android.view.View
import android.view.animation.AccelerateInterpolator
import revolut.test.util.simplelisteners.OnEndAnimatorListener

class RateItemAnimator(private val recyclerView: RecyclerView) : DefaultItemAnimator() {

    companion object {
        const val ANIMATION_DURATION_DEFAULT = 500L
    }

    private var slideAnimationStartListener: (() -> Unit)? = null
    private var slideAnimationFinishListener: (() -> Unit)? = null

    init {
        moveDuration = ANIMATION_DURATION_DEFAULT
    }

    override fun animateMove(holder: RecyclerView.ViewHolder?, fromX: Int, fromY: Int, toX: Int, toY: Int): Boolean {
        if (holder != null && fromY != toY) {
            return (holder.itemView.getTag(RateViewHolder.KEY_CLICKED) as? Boolean)?.let {
                holder.itemView.setTag(RateViewHolder.KEY_CLICKED, false)
                animateSlideTop(holder, fromY, toY)
            } ?: super.animateMove(holder, fromX, fromY, toX, toY)
        }
        return super.animateMove(holder, fromX, fromY, toX, toY)
    }

    private fun animateSlideTop(holder: RecyclerView.ViewHolder, fromY: Int, toY: Int): Boolean {
        //to make slide over effect
        RecyclerProxy.smoothScrollBy(
                recyclerView = recyclerView,
                dx = 0,
                dy = -(holder.itemView.measuredWidth + recyclerView.computeVerticalScrollOffset()) - 100,
                duration = ANIMATION_DURATION_DEFAULT.toInt(),
                interpolator = AccelerateInterpolator()
        )
        holder.itemView.elevation = 1f
        dispatchMoveStarting(holder)
        slideAnimationStartListener?.invoke()

        val objectAnimator = ObjectAnimator.ofObject(
                /* object        */ holder.itemView,
                /* property      */ Property.of(View::class.java, Float::class.java, "translationY"),
                /* typeEvaluator */ FloatEvaluator(),
                /* from          */ fromY.toFloat() + holder.itemView.measuredHeight,
                /* to            */ 0f
        )
        with(objectAnimator) {
            duration = ANIMATION_DURATION_DEFAULT
            interpolator = AccelerateInterpolator()
            addListener(OnEndAnimatorListener {
                holder.itemView.elevation = 0f
                dispatchMoveFinished(holder)
                slideAnimationFinishListener?.invoke()
            })
            start()
        }
        return true
    }

    fun subscribeToSlideAnimation(startListener: () -> Unit, finishListener: () -> Unit) {
        slideAnimationStartListener = startListener
        slideAnimationFinishListener = finishListener
    }

    class FloatEvaluator : TypeEvaluator<Float> {
        override fun evaluate(fraction: Float, startValue: Float?, endValue: Float?): Float {
            startValue ?: return 0f
            endValue ?: return 0f
            return startValue + fraction * (endValue - startValue)
        }

    }

}