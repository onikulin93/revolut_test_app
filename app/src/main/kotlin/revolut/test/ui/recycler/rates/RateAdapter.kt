package revolut.test.ui.recycler.rates

import android.os.Bundle
import android.support.v7.recyclerview.extensions.AsyncDifferConfig
import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import revolut.test.R
import revolut.test.ui.listener.FocusAndAmountListener
import revolut.test.uimodel.RateItem
import kotlinx.android.synthetic.main.item_rate.view.item_rate_edit_field as rateEditField
import kotlinx.android.synthetic.main.item_rate.view.item_rate_image as rateImageView
import kotlinx.android.synthetic.main.item_rate.view.item_rate_info as rateInfoView
import kotlinx.android.synthetic.main.item_rate.view.item_rate_name as rateNameView

class RateAdapter(
        private val firstRateFocusAndAmountListener: FocusAndAmountListener,
        private val otherRatesClickListener: (RateItem) -> Unit
) : RecyclerView.Adapter<RateViewHolder>() {

    companion object {

        const val KEY_RATE_CHANGED_TO = ".key_rate_changed"
        const val KEY_AMOUNT_CHANGED_TO = ".key_amount_changed_to"
    }

    internal var rateListUpdateCallback = RateListUpdateCallback(this)

    private val asyncDiffer = AsyncListDiffer(
            rateListUpdateCallback,
            AsyncDifferConfig.Builder<RateItem>(RateItemDiffCallback()).build()
    )

    fun onItemsChanged(items: List<RateItem>) {
        asyncDiffer.submitList(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            RateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false))

    override fun getItemCount() = asyncDiffer.currentList.size

    override fun onBindViewHolder(holder: RateViewHolder, position: Int, payloads: MutableList<Any>) {
        val item = asyncDiffer.currentList[position]

        payloads
                .firstOrNull()
                ?.let { it as? Bundle }
                ?.takeUnless(Bundle::isEmpty)
                ?.let(holder::partialBind)
                ?: (holder::fullBind)(item)

        if (position == 0) {
            holder.unbindClickListeners()
            holder.bindFocusAndRateListener(firstRateFocusAndAmountListener)
        } else {
            holder.bindClickListener(item, otherRatesClickListener)
        }
    }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) = Unit

    class RateItemDiffCallback : DiffUtil.ItemCallback<RateItem>() {

        override fun areItemsTheSame(oldItem: RateItem?, newItem: RateItem?): Boolean {
            oldItem ?: return false
            newItem ?: return false
            return oldItem.rate.name == newItem.rate.name
        }

        override fun areContentsTheSame(oldItem: RateItem?, newItem: RateItem?): Boolean {
            oldItem ?: return false
            newItem ?: return false
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: RateItem?, newItem: RateItem?): Any? {
            oldItem ?: return null
            newItem ?: return null
            val bundle = Bundle()
            if (oldItem.rate.rate != newItem.rate.rate || oldItem.amount != newItem.amount) {
                bundle.putFloat(KEY_RATE_CHANGED_TO, newItem.rate.rate)
                bundle.putFloat(KEY_AMOUNT_CHANGED_TO, newItem.amount)
            }
            return if (bundle.isEmpty) null else bundle
        }
    }
}