package revolut.test.ui.fragment.ext

import android.support.v4.app.Fragment
import kotlin.reflect.KProperty

class FloatArgumentProperty(
        private val key: String,
        private val fallback: Float
) : BundleArgumentProperty<Float>() {

    override fun getValue(thisRef: Fragment, property: KProperty<*>): Float {
        return thisRef.getBundle().getFloat(key, fallback)
    }

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: Float) {
        thisRef.getBundle().putFloat(key, value)
    }
}