package revolut.test.ui.fragment.ext

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity

fun FragmentActivity.addFragmentToBackStack(@IdRes containerViewId: Int,
                                            fragment: Fragment,
                                            tag: String,
                                            executeNow: Boolean = true,
                                            addToBackStack: Boolean = true) {
    with(this.supportFragmentManager) {
        beginTransaction()
                .apply {
                    if (addToBackStack) addToBackStack(tag)
                }
                .add(containerViewId, fragment, tag)
                .commit()
        if (executeNow) executePendingTransactions()
    }
}