package revolut.test.ui.fragment.ext

import android.os.Bundle
import android.support.v4.app.Fragment
import kotlin.properties.ReadWriteProperty

//main purpose is to store scenario wide values, which should be available after process death
abstract class BundleArgumentProperty<R> : ReadWriteProperty<Fragment, R> {

    @Synchronized
    protected fun Fragment.getBundle(): Bundle {
        return arguments ?: {
            val newBundle = Bundle()
            arguments = newBundle
            newBundle
        }()
    }

}