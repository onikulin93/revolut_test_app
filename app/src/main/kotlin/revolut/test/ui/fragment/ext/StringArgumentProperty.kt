package revolut.test.ui.fragment.ext

import android.support.v4.app.Fragment
import kotlin.reflect.KProperty

class StringArgumentProperty(
        private val key: String,
        private val fallback: String
) : BundleArgumentProperty<String>() {

    override fun getValue(thisRef: Fragment, property: KProperty<*>): String {
        return thisRef.getBundle().getString(key, fallback)
    }

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: String) {
        thisRef.getBundle().putString(key, value)
    }
}