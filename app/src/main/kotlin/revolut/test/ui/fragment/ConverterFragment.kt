package revolut.test.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.FixedItemSizeLinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.item_rate.view.*
import revolut.test.R
import revolut.test.ui.fragment.ext.FloatArgumentProperty
import revolut.test.ui.fragment.ext.StringArgumentProperty
import revolut.test.ui.listener.FocusAndAmountListener
import revolut.test.ui.presenter.ConverterPresenter
import revolut.test.ui.recycler.rates.RateAdapter
import revolut.test.ui.recycler.rates.RateItemAnimator
import revolut.test.ui.view.makeGone
import revolut.test.ui.view.makeVisible
import revolut.test.ui.viewinterface.ConverterView
import revolut.test.uimodel.RateItem
import revolut.test.util.setKeyboardFocusView
import kotlinx.android.synthetic.main.fragment_converter.fragment_converter_recyclerview as recyclerView
import kotlinx.android.synthetic.main.fragment_converter.fragment_converter_click_overlay as clickOverlay
import kotlinx.android.synthetic.main.fragment_converter.fragment_converter_toolbar as toolbarContainer
import kotlinx.android.synthetic.main.toolbar.toolbar_title as toolbarTitleView

class ConverterFragment : MvpAppCompatFragment(), ConverterView {

    companion object {

        const val TAG = "ConverterFragment"

        private const val KEY_CURRENT_RATE_NAME = ".key_current_rate_name"
        private const val KEY_CURRENT_RATE_AMOUNT = ".key_current_rate_amount"

        private const val DEFAULT_RATE = "GBP"
        private const val DEFAULT_AMOUNT = 100f

        fun newInstance(): Fragment {
            return ConverterFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_CURRENT_RATE_NAME, DEFAULT_RATE)
                    putFloat(KEY_CURRENT_RATE_AMOUNT, DEFAULT_AMOUNT)
                }
            }
        }
    }

    @InjectPresenter
    internal lateinit var converterPresenter: ConverterPresenter

    private var rateName by StringArgumentProperty(KEY_CURRENT_RATE_NAME, DEFAULT_RATE)
    private var rateMultiplier by FloatArgumentProperty(KEY_CURRENT_RATE_AMOUNT, DEFAULT_AMOUNT)

    @ProvidePresenter
    internal fun provideConverterPresenter() = ConverterPresenter(
            rateName = rateName,
            rateMultiplier = rateMultiplier
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_converter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initRecyclerView()
    }

    override fun subscribeToSlideAnimation(rateItem: RateItem) {
        (recyclerView.itemAnimator as? RateItemAnimator)?.let {
            it.subscribeToSlideAnimation(
                    startListener = {
                        clickOverlay.makeVisible()
                    },
                    finishListener = {
                        clickOverlay.makeGone()
                        rateName = rateItem.rate.name
                        rateMultiplier = rateItem.rate.rate * rateItem.amount
                        converterPresenter.onBaseRateChangeFinished(rateName, rateMultiplier)
                    }
            )
        }
    }

    @Volatile
    private var firstTime = true

    override fun updateRateList(rateList: List<RateItem>) {
        (recyclerView.adapter as? RateAdapter)?.onItemsChanged(rateList)
        if (firstTime) {
            firstTime = false
            recyclerView.postDelayed({
                val child = recyclerView.getChildAt(0)
                child.item_rate_edit_field.setKeyboardFocusView(
                        100,
                        child.measuredWidth - 10f,
                        0f
                )
            }, 250L)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initRecyclerView() {
        with(context ?: return) {

            recyclerView.itemAnimator = RateItemAnimator(recyclerView)
            recyclerView.layoutManager = FixedItemSizeLinearLayoutManager(
                    /* context */        this,
                    /* orientation */    RecyclerView.VERTICAL,
                    /* reverseLayout */  false
            )

            val adapter = RateAdapter(
                    firstRateFocusAndAmountListener = object : FocusAndAmountListener {

                        override fun onAmountChanged(amount: Float) {
                            converterPresenter.onMultiplierChanged(amount)
                        }

                        override fun onFocusChange(v: View?, hasFocus: Boolean) {

                            (recyclerView.adapter as? RateAdapter)
                                    ?.rateListUpdateCallback
                                    ?.isFirstItemFreezed = hasFocus


                            //if (!hasFocus) {to scrolllistener
                            //    activity.hideKeyboard()
                            //}
                        }
                    },
                    otherRatesClickListener = { rateItem: RateItem ->
                        converterPresenter.onBaseRateChangeStart(rateItem)
                    }
            )
            recyclerView.adapter = adapter
        }
    }

    private fun initToolbar() {
        context?.let { context ->
            with(toolbarContainer) {
                toolbarTitleView.text = context.getString(R.string.fragment_converter_title)
            }
        }
    }
}