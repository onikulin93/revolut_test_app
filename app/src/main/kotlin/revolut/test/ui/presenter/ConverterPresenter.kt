package revolut.test.ui.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import revolut.test.di.DI
import revolut.test.domain.dataprovider.ResourceManager
import revolut.test.domain.interactor.ratesconverter.RatesConverterInteractor
import revolut.test.domain.interactor.ratesconverter.UpdateInfo
import revolut.test.domain.model.Rate
import revolut.test.ui.viewinterface.ConverterView
import revolut.test.uimodel.RateInfo
import revolut.test.uimodel.RateItem
import revolut.test.util.CurrencyUtil
import javax.inject.Inject

@InjectViewState
class ConverterPresenter(rateName: String, rateMultiplier: Float) : MvpPresenter<ConverterView>() {

    @Inject
    internal lateinit var ratesConverterInteractor: RatesConverterInteractor

    @Inject
    internal lateinit var resourceManager: ResourceManager

    private val ratesComponentHolder by lazy {
        DI.app.provideComponent()
                .ratesComponentBuilder()
                .rateMultiplier(rateMultiplier)
                .baseRateName(rateName)
                .build()
    }

    private val compositeDisposable by lazy { CompositeDisposable() }
    private var updateDisposable: Disposable? = null

    init {
        ratesComponentHolder.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        subscribeToUpdates()
    }

    fun onMultiplierChanged(multiplier: Float) {
        ratesConverterInteractor.onRateMultiplierChanged(multiplier)
    }

    fun onBaseRateChangeFinished(baseRate: String, rateMultiplier: Float) {
        ratesConverterInteractor.onBaseRateChanged(baseRate, rateMultiplier)
        subscribeToUpdates()
    }

    fun onBaseRateChangeStart(rateItem: RateItem) {
        safeSubscribe {
            ratesConverterInteractor.stopUpdates()
                    .doOnSuccess { ratesConverterInteractor.constructPositionsMap(
                            newLeadingRate = rateItem.rate,
                            oldLeadingRate = it.heading,
                            oldRateList = it.rateList
                    )
                    }
                    .map { rateListWithNewLeading(rateItem, it) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        updateDisposable?.dispose()
                        viewState.subscribeToSlideAnimation(rateItem)
                        Log.d("rateUpdateMapper", "1 submittedList ${it.map { it.rate.name }}")
                        viewState.updateRateList(it)
                    }, {
                        Log.d("", "")
                    })
        }
    }

    private fun subscribeToUpdates() {
        updateDisposable = safeSubscribe {
            ratesConverterInteractor.updates()
                    .map { rateListFromUpdateInfo(it) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        updateDisposable?.let {
                            if (it.isDisposed) {
                                return@subscribe
                            }
                        }
                        Log.d("rateUpdateMapper", "2 submittedList ${it.map { it.rate.name }}")
                        viewState.updateRateList(it)
                    }, {

                    })
        }
    }

    private fun rateListWithNewLeading(newLeadingItem: RateItem, updateInfo: UpdateInfo): List<RateItem> {
        var rList = rateListFromUpdateInfo(updateInfo)
        val newHeading = rList.find { it.rate.name == newLeadingItem.rate.name } ?: return rList
        rList = rList.toMutableList().apply { remove(newHeading) }
        return listOf(newHeading) + rList
    }

    private fun rateListFromUpdateInfo(updateInfo: UpdateInfo): List<RateItem> {
        val heading = rateToRateItem(updateInfo.heading,updateInfo.multiplier)
        val other = updateInfo.rateList.map { rateToRateItem(it, updateInfo.multiplier) }
        return listOf(heading) + other
    }

    private fun rateToRateItem(rate: Rate, multiplier: Float): RateItem {
        return RateItem(
                amount = rate.rate * multiplier,
                rate = rate,
                rateInfo = RateInfo(
                        description = CurrencyUtil.getDescriptionByName(rate.name),
                        imageResource = CurrencyUtil.getResourceImageByRateName(rate.name)
                )
        )
    }

    private fun safeSubscribe(action: () -> Disposable): Disposable {
        val disposable = action()
        compositeDisposable.add(disposable)
        return disposable
    }
}