package revolut.test.di.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import revolut.test.di.module.ModelModule
import revolut.test.di.module.NetworkModule
import revolut.test.di.scope.ApplicationScope
import revolut.test.di.subcomponent.RatesSubComponent

@ApplicationScope
@Component(modules = [NetworkModule::class, ModelModule::class])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): ApplicationComponent
    }

    fun ratesComponentBuilder(): RatesSubComponent.Builder
}