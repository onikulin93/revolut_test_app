package revolut.test.di.module.rates

import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.joda.time.DateTime
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import revolut.test.data.network.ApiFactory
import revolut.test.data.dataprovider.RatesDataRepository
import revolut.test.data.model.NWRateList
import revolut.test.data.network.api.RatesApi
import revolut.test.data.network.deserializer.DateTimeDeserializer
import revolut.test.data.network.deserializer.RateListDeserializer
import revolut.test.di.qualifier.BaseUrlQualifier
import revolut.test.di.qualifier.RatesApiFactory
import revolut.test.di.qualifier.RatesConverterFactoryQualifier
import revolut.test.domain.dataprovider.RatesRepository

@Module
abstract class RatesModelModule {

    @Binds
    abstract fun bindRatesRepository(ratesDataRepository: RatesDataRepository): RatesRepository

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideRatesApi(@RatesApiFactory apiFactory: ApiFactory): RatesApi {
            return apiFactory.create(RatesApi::class.java)
        }

        @RatesConverterFactoryQualifier
        @JvmStatic
        @Provides
        fun provideConverterFactory(): Converter.Factory {
            return GsonConverterFactory.create(
                    GsonBuilder()
                            .registerTypeAdapter(DateTime::class.java, DateTimeDeserializer())
                            .registerTypeAdapter(NWRateList::class.java, RateListDeserializer())
                            .create()
            )
        }

        @RatesApiFactory
        @JvmStatic
        @Provides
        fun provideApiFactory(
                @BaseUrlQualifier baseUrl: String,
                okHttpClient: OkHttpClient,
                @RatesConverterFactoryQualifier converterFactory: Converter.Factory,
                callAdapterFactory: CallAdapter.Factory): ApiFactory = ApiFactory(baseUrl, okHttpClient, converterFactory, callAdapterFactory)
    }
}