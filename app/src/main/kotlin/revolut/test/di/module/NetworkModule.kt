package revolut.test.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import revolut.test.BuildConfig
import revolut.test.data.model.NWRateList
import revolut.test.data.network.ApiFactory
import revolut.test.data.network.deserializer.RateListDeserializer
import revolut.test.di.qualifier.BaseUrlQualifier

@Module
class NetworkModule {

    @BaseUrlQualifier
    @Provides
    fun provideBaseServerUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create(Gson())
    }

    @Provides
    fun provideCallAdapterFactory(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient = with(OkHttpClient.Builder()) {
        addInterceptor(HttpLoggingInterceptor())
        build()
    }

    @Provides
    fun provideApiFactory(
            @BaseUrlQualifier baseUrl: String,
            okHttpClient: OkHttpClient,
            converterFactory: Converter.Factory,
            callAdapterFactory: CallAdapter.Factory)
            = ApiFactory(baseUrl, okHttpClient, converterFactory, callAdapterFactory)
}