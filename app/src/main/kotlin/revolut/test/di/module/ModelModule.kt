package revolut.test.di.module

import dagger.Binds
import dagger.Module
import revolut.test.data.dataprovider.ResourceDataManager
import revolut.test.domain.dataprovider.ResourceManager

@Module
abstract class ModelModule {

    @Binds
    abstract fun bindsResourceManager(resourceDataManager: ResourceDataManager): ResourceManager

}