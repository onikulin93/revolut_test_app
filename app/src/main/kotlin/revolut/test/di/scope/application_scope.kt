package revolut.test.di.scope

import javax.inject.Singleton

typealias ApplicationScope = Singleton