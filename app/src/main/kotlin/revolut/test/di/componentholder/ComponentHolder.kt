package revolut.test.di.componentholder

abstract class ComponentHolder<Component>(private val provider: () -> Component) {

    private var innerComponent: Component? = null//todo atomic reference?

    @Synchronized
    fun provideComponent(): Component {
        val local = innerComponent
        val component: Component
        if (local == null) {
            component = provider.invoke()
            innerComponent = component
        } else {
            component = local
        }
        return component
    }

    @Synchronized
    fun onDependencyReleased() {
        innerComponent = null
    }
}