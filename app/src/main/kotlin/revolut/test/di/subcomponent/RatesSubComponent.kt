package revolut.test.di.subcomponent

import dagger.BindsInstance
import dagger.Subcomponent
import revolut.test.di.module.rates.RatesModelModule
import revolut.test.di.scope.RatesScope
import revolut.test.domain.interactor.ratesconverter.BaseRateQualifier
import revolut.test.domain.interactor.ratesconverter.RateMultiplier
import revolut.test.ui.presenter.ConverterPresenter

@RatesScope
@Subcomponent(modules = [RatesModelModule::class])
interface RatesSubComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun rateMultiplier(@RateMultiplier multiplier: Float): Builder

        @BindsInstance
        fun baseRateName(@BaseRateQualifier base: String): Builder

        fun build(): RatesSubComponent
    }

    fun inject(target: ConverterPresenter)
}