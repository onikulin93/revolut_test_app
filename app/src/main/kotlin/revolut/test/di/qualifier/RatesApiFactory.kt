package revolut.test.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class RatesApiFactory