package revolut.test.di

import android.annotation.SuppressLint
import android.content.Context
import revolut.test.di.component.ApplicationComponent
import revolut.test.di.component.DaggerApplicationComponent
import revolut.test.di.componentholder.ComponentHolder

@SuppressLint("StaticFieldLeak")
object DI {

    val app by lazy { ApplicationComponentHolder() }

    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    class ApplicationComponentHolder : ComponentHolder<ApplicationComponent>({
        DaggerApplicationComponent
                .builder()
                .context(context)
                .build()
    })
}