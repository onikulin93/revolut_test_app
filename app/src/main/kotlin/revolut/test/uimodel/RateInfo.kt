package revolut.test.uimodel

data class RateInfo(
        val description: Int,
        val imageResource: Int
)