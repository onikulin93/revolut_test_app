package revolut.test.uimodel

import revolut.test.domain.model.Rate

data class RateItem(
        val amount: Float,
        val rate: Rate,
        val rateInfo: RateInfo
)