package revolut.test.util.simplelisteners

import android.animation.Animator

class OnEndAnimatorListener(private val onEndFunction: () -> Unit) : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) = Unit

    override fun onAnimationEnd(animation: Animator?) = onEndFunction.invoke()

    override fun onAnimationCancel(animation: Animator?) = Unit

    override fun onAnimationStart(animation: Animator?) = Unit
}