package revolut.test.util.simplelisteners

import android.text.Editable
import android.text.TextWatcher

open class SimpleTextWatcher(private val listener: (String) -> Unit) : TextWatcher {

    var first = true

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

    override fun afterTextChanged(s: Editable?) {
        if (first) {
            first = false
        }
        s?.let { text ->
            listener.invoke(text.toString())
        }
    }
}