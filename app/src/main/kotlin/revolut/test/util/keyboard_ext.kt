package revolut.test.util

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.SystemClock
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Activity?.hideKeyboard() {
    with(this ?: return) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.findViewById<View>(android.R.id.content).windowToken, 0)
    }
}

fun View.setKeyboardFocusViewImmediate() {
    requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(findFocus(), InputMethodManager.SHOW_IMPLICIT)
}

fun View.setKeyboardFocusView(delay: Long = 250L, xPosition: Float = 0f, yPosition: Float = 0f) {
    postDelayed({
        dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
                MotionEvent.ACTION_DOWN, xPosition, yPosition, 0))
        dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
                MotionEvent.ACTION_UP, xPosition, yPosition, 0))
    }, delay)
}