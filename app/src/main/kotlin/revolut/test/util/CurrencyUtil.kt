package revolut.test.util

import android.content.Context
import revolut.test.R


object CurrencyUtil {

    fun getResourceImageByRateName(name: String): Int {
        return when (name.toLowerCase()) {
            "aud" -> R.drawable.c_aud
            "brl" -> R.drawable.c_brl
            "bgn" -> R.drawable.c_bgn
            "cad" -> R.drawable.c_cad
            "chf" -> R.drawable.c_chf
            "cny" -> R.drawable.c_cny
            "czk" -> R.drawable.c_czk
            "dkk" -> R.drawable.c_dkk
            "eur" -> R.drawable.c_eur
            "gbp" -> R.drawable.c_gb
            "hkd" -> R.drawable.c_hkd
            "hrk" -> R.drawable.c_hrk
            "huf" -> R.drawable.c_huf
            "idr" -> R.drawable.c_idr
            "ils" -> R.drawable.c_ils
            "inr" -> R.drawable.c_inr
            "jpy" -> R.drawable.c_jpy
            "krw" -> R.drawable.c_krw
            "mxn" -> R.drawable.c_mxn
            "myr" -> R.drawable.c_myr
            "nok" -> R.drawable.c_nok
            "nzd" -> R.drawable.c_nzd
            "php" -> R.drawable.c_php
            "pln" -> R.drawable.c_pln
            "ron" -> R.drawable.c_ron
            "rub" -> R.drawable.c_rub
            "sek" -> R.drawable.c_sek
            "sgd" -> R.drawable.c_sgd
            "thb" -> R.drawable.c_thb
            "try" -> R.drawable.c_try
            "usd" -> R.drawable.c_usd
            "zar" -> R.drawable.c_zar
            else -> R.drawable.c_fallback
        }
    }

    fun getDescriptionByName(name: String): Int {
        return when (name.toLowerCase()) {
            "aud" -> R.string.c_aud
            "brl" -> R.string.c_brl
            "bgn" -> R.string.c_bgn
            "cad" -> R.string.c_cad
            "chf" -> R.string.c_chf
            "cny" -> R.string.c_cny
            "czk" -> R.string.c_czk
            "dkk" -> R.string.c_dkk
            "eur" -> R.string.c_eur
            "gbp" -> R.string.c_gbp
            "hkd" -> R.string.c_hkd
            "hrk" -> R.string.c_hrk
            "huf" -> R.string.c_huf
            "idr" -> R.string.c_idr
            "ils" -> R.string.c_ils
            "inr" -> R.string.c_inr
            "jpy" -> R.string.c_jpy
            "krw" -> R.string.c_krw
            "mxn" -> R.string.c_mxn
            "myr" -> R.string.c_myr
            "nok" -> R.string.c_nok
            "nzd" -> R.string.c_nzd
            "php" -> R.string.c_php
            "pln" -> R.string.c_pln
            "ron" -> R.string.c_ron
            "rub" -> R.string.c_rub
            "sek" -> R.string.c_sek
            "sgd" -> R.string.c_sgd
            "thb" -> R.string.c_thb
            "try" -> R.string.c_try
            "usd" -> R.string.c_usd
            "zar" -> R.string.c_zar
            else -> R.string.c_fallback
        }
    }

}