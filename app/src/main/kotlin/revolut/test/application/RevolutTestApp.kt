package revolut.test.application

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid
import revolut.test.di.DI

class RevolutTestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        DI.init(this)
        JodaTimeAndroid.init(this);
    }
}